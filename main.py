import json
import netifaces
import time

import requests
from geopy.distance import distance

import iwlist

METRO_STATIONS = {
    'Лісова': (50.464826, 30.644037),
    'Чернігівська': (50.459809, 30.629820),
    'Дарниця': (50.459809, 30.629820),
}

SETTINGS = {
    'ping_period': 3,
}

DESTINATION = 'Лісова'
WAKE_MINUTES_BEFORE_ARRIVAL = 2

AVG_TRAIN_VELOCITY_KM_PER_HOUR = 50
MIN_DISTANCE = AVG_TRAIN_VELOCITY_KM_PER_HOUR / 60 * WAKE_MINUTES_BEFORE_ARRIVAL

interfaces = netifaces.interfaces()
ip = iwlist.get_ip()


def get_coordinates():
    mac_addresses = []
    for interface in interfaces:
        content = iwlist.scan(interface)
        cells = iwlist.parse(content)
        if cells:
            for cell in cells:
                mac_addresses.append({'mac': cell['mac']})
    data = {
        "common": {
            "version": "1.0",
            "api_key": "AKBmg1cBAAAAunBKbAIAIIS3jBIvzC1euqHu296B0Y5GvFEAAAAAAAAAAACX2qBiWPdsmHd5XfTAoc_kGa_v-w=="
        },
        "wifi_networks": mac_addresses,
        "ip": {
            "address_v4": ip
        }
    }
    print(data)
    data = json.dumps(data)
    r = requests.post('http://api.lbs.yandex.net/geolocation', data={'json': data})
    print(r)
    result = r.json()['position']
    return result['latitude'], result['longitude']


def wake_up():
    pass


if __name__ == '__main__':
    # DESTINATION = input('Станція, на яку ви їдете: ')
    # WAKE_MINUTES_BEFORE_ARRIVAL = input('За скільки хвилин до приїзду вас розбудити: ')
    arrival = False
    while not arrival:
        coordinates = get_coordinates()
        dist = distance(coordinates, METRO_STATIONS[DESTINATION]).kilometers
        if dist <= MIN_DISTANCE:
            arrival = True
            wake_up()
        else:
            time.sleep(SETTINGS['ping_period'])
